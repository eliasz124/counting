# Räknespel tillhörande Elias Rosenkvist Te3a

## Spelet går ut på att användaren får svara på ett antal matematiska frågor med olika räknesätt. Användaren får själv välja vilket räknesätt samt hur många frågor och av vilken svårighetsgrad som frågorna ska ställas.

### För att starta spelet krävs det att man går in i cmd, alternativt terminalen för linux användare. Därefter anger cd + (sökvägen där spelet befinner sig). Sedan skriver man "node index.js --räknesätt + (a,s,m,d) --frågor + (antalet frågor) --svårighetsgrad + (svårighetsgraden)". Se exemplet nedan som visar 10 multiplikationsfrågor med svårighetsgraden 5. 

### Exempel:
### node index.js --räknesätt m --frågor 10 --svårighetsgrad 5

### De olika operatorerna man kan välja är m (multiplikation), a (addition), s (subtraktion) samt d (division).

## Slutligen så kommer resultatet att sparas i filen .travis.yml.
